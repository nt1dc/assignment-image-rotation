//
// Created by nt1dc on 10.02.2022.
//
#include "image.h"
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height) {
    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc(sizeof(struct pixel) * height * width);
    if (image.data == NULL) {
        printf("ERROR: in memory allocation");
        abort();
    }
    return image;
}

void image_destroy(struct image *img) {
    free(img->data);
}
