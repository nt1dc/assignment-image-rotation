#include "io_for_file.h"


FILE *open_read(const char *path) {
    return fopen(path, "rb");
}

FILE *open_write(const char *path) {
    return fopen(path, "wb");
}

enum io_status close(FILE *file) {
    if (fclose(file) == 0) {
        return OK;
    }
    return BAD;
}
