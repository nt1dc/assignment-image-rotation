//
// Created by nt1dc on 10.02.2022.
//
#include "bmp.h"


static uint8_t calc_padding(const size_t width) {
    return DWORD_SIZE - (width * sizeof(struct pixel)) % DWORD_SIZE;
}

static enum read_status read_header(FILE *in, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

static enum read_status read_image(FILE *in, struct image *img) {
    for (uint32_t i = 0; i < img->height; i++) {
        if (fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in)
            < img->width || fseek(in, calc_padding(img->width), SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    enum read_status status = read_header(in, &header);

    *img = image_create(header.biWidth, header.biHeight);
//    img->data = malloc(sizeof(struct pixel) * header.biHeight * header.biWidth);
//    img->width = header.biWidth;
//    img->height = header.biHeight;
    if (status != READ_OK) {
        return status;
    }
    status = read_image(in, img);
    if (status != READ_OK) {
        return status;
    }
    return READ_OK;
}

static void create_header(struct bmp_header *header, const size_t width, const size_t height) {
    const uint8_t padding = calc_padding(width);
    const size_t image_size = (sizeof(struct pixel) * width + padding) * height;
    header->bfType = BMP_TYPE;
    header->biBitCount = BMP_VALID_BIT_COUNT;
    header->biXPelsPerMeter = BMP_PIXEL_PER_METER;
    header->biYPelsPerMeter = BMP_PIXEL_PER_METER;
    header->bfileSize = image_size + sizeof(struct bmp_header);
    header->bfReserved = BMP_RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BMP_HEADER_SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BMP_PLANES;
    header->biCompression = BMP_COMPRESSION;
    header->biSizeImage = image_size;
    header->biClrUsed = BMP_CIR_USED;
    header->biClrImportant = BMP_CIR_IMPORTANT;
}


enum write_status to_bmp(FILE *out, struct image const *img) {
//    const struct bmp_header *header = malloc(sizeof(struct bmp_header));
    struct bmp_header header = {0};
//    if (header == NULL) return WRITE_ERROR;
    create_header((struct bmp_header *) &header, img->width, img->height);

    if (fwrite(&header, sizeof(struct bmp_header), 1, (FILE *) out) != 1) {
//        free((struct bmp_header *) header);
        return WRITE_ERROR;
    }


    const size_t width = img->width;
    const size_t height = img->height;
    const uint8_t padding = calc_padding(width);
//    uint8_t *null_bits = calloc(1, padding);
//    if (null_bits == NULL) {
//        free((struct bmp_header *) header);
//        return WRITE_ERROR;
//    }
    const uint8_t null_bits[3] = {0};
    for (uint32_t i = 0; i < height; i++) {
        if (fwrite(img->data + width * i, width * sizeof(struct pixel), 1, (FILE *) out) != 1) {
//            free((struct bmp_header *) header);
//            free(null_bits);
            return WRITE_ERROR;
        }
        if (fwrite(null_bits, padding, 1, (FILE *) out) != 1) {
//            free((struct bmp_header *) header);
//            free(null_bits);
            return WRITE_ERROR;
        }
    }
//    free((struct bmp_header *) header);
//    free(null_bits);
    return WRITE_OK;
}



