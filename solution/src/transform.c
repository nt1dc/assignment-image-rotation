//
// Created by nt1dc on 10.02.2022.
//
#include "transform.h"

extern struct image rotate(struct image const *source) {

    struct image img = image_create(source->height, source->width);
//    img.data = malloc(sizeof(struct pixel) * source->height * source->width);
    for (size_t i = 0; i < source->height; i++) {
        for (size_t j = 0; j < source->width; j++) {
            img.data[source->height - i - 1 + j * source->height] = source->data[i * source->width + j];
        }
    }
    return  img;

}

