#include "bmp.h"
#include "io_for_file.h"
#include "transform.h"
#include <stdio.h>


int main(int argc, char **argv) {
    if (argc != 3) {
        printf("2 args required\n");
        return -1;
    }
    char *input = argv[1];
    char *output = argv[2];
    FILE *input_file = open_read(input);
    struct image input_image ={0};
    enum read_status status = from_bmp(input_file, &input_image);
    close(input_file);
    if (status != READ_OK) {
        printf("READ NO OK )_))) )) \n");
        image_destroy(&input_image);
        return -1;
    }

    struct image output_image = rotate(&input_image);
    image_destroy(&input_image);

    FILE *output_file = open_write(output);
    enum write_status w_status = to_bmp(output_file, &output_image);
    image_destroy(&output_image);
    close(output_file);
    if (w_status != WRITE_OK) {
        printf("WRITE NOT OK)) )) ) ) )\n");
        return -1;
    }

    printf("CHECK UR IMAGE HERE \n");
    printf("%s\n", output);
    return 0;
}
