//
// Created by nt1dc on 09.02.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>


struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image image_create(uint64_t width, uint64_t height);

void image_destroy(struct image *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
