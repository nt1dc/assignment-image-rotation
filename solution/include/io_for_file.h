//
// Created by nt1dc on 09.02.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_FOR_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_FOR_FILE_H

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

enum io_status {
    OK = 0,
    BAD = 1
};

FILE *open_read(const char *path);

FILE *open_write(const char *path);

enum io_status close(FILE *file);

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_FOR_FILE_H
