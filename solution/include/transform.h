//
// Created by nt1dc on 10.02.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H

#include "image.h"
#include <stdio.h>

enum transformation_status {
    TRANSFORMATION_OK = 0,
    TRANSFORMATION_ERROR
};

struct image rotate(struct image const *source);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
